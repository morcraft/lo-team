const _ = require('lodash')
const hide = require('./hide.js')

module.exports = function(args){
    if(!_.isObject(args))
        throw new Error('Invalid arguments')

    if(!_.isObject(args.instance))
        throw new Error('Invalid instance')

    if(!(args.element instanceof Element))
        throw new Error('Invalid element')

    args.element.addEventListener('click', function(){
        args.instance.toggle()
    })
    
    return args.element
}