const _ = require('lodash')

module.exports = function(args){
    if(!_.isObject(args))
        throw new Error('Invalid arguments')

    if(!_.isObject(args.instance))
        throw new Error('Invalid instance')

    _.forEach(args.instance.props.elements.trees, function(element){
        if(_.includes(element.classList, 'concealed')){
            element.classList.remove('concealed')
            args.instance.events.emit('teamTreeShow', {
                element: element
            })
        }
        else{
            element.classList.add('concealed')
            args.instance.events.emit('teamTreeHide', {
                element: element
            })
        }
    })

    return true
}