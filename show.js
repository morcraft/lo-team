const _ = require('lodash')

module.exports = function(args){
    if(!_.isObject(args))
        throw new Error('Invalid arguments')

    if(!_.isObject(args.instance))
        throw new Error('Invalid instance')

    _.forEach(args.instance.props.elements.trees, function(element){
        element.classList.remove('concealed')
        args.instance.events.emit('teamTreeShow', {
            element: element
        })
    })

    return true
}