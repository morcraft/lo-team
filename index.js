const _ = require('lodash')
const renderTeamTree = require('./renderTeamTree.js')
const renderTeamIcon = require('./renderTeamIcon.js')
const props = require('./props.js')
const toggle = require('./toggle.js')
const show = require('./show.js')
const hide = require('./hide.js')
const EventsEmitter = require('events')

module.exports = function(args){
    var self = this
    self.props = new props(args)
    self.events = new EventsEmitter()

    self.toggle = function(_args){
        return toggle(_.merge({
            instance: self,
        }, _args))
    }

    self.show = function(_args){
        return show(_.merge({
            instance: self
        }, _args))
    }

    self.hide = function(_args){
        return hide(_.merge({
            instance: self
        }, _args))
    }

    self.renderTeamIcon = function(_args){
        return renderTeamIcon(_.merge({
            instance: self
        }, _args))
    }

    self.renderTeamTree = function(_args){
        return renderTeamTree(_.merge({
            instance: self
        }, _args))
    }
}