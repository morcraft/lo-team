(function(){function teamTree(it
/**/) {
var out='<div class="team-modal"> <div class="close-button hoverable"> <i class="fa fa-angle-down"></i> </div> <div class="header"> <div class="title"> <h2>'+(it.treeName)+'</h2> </div> </div> <div class="body">'+(it.team)+'</div></div>';return out;
}var itself=teamTree, _encodeHTML=(function (doNotSkipEncoded) {
		var encodeHTMLRules = { "&": "&#38;", "<": "&#60;", ">": "&#62;", '"': "&#34;", "'": "&#39;", "/": "&#47;" },
			matchHTML = doNotSkipEncoded ? /[&<>"'\/]/g : /&(?!#?\w+;)|<|>|"|'|\//g;
		return function(code) {
			return code ? code.toString().replace(matchHTML, function(m) {return encodeHTMLRules[m] || m;}) : "";
		};
	}());if(typeof module!=='undefined' && module.exports) module.exports=itself;else if(typeof define==='function')define(function(){return itself;});else {window.render=window.render||{};window.render['teamTree']=itself;}}());