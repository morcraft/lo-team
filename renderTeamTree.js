const _ = require('lodash')
const htmlToElement = require('./htmlToElement.js')
const teamMember = require('./templates/teamMember.js')
const teamGroup = require('./templates/teamGroup.js')
const teamTree = require('./templates/teamTree.js')
const addTeamTreeEvents = require('./addTeamTreeEvents.js')

module.exports = function(args){
    if(!_.isObject(args))
        throw new Error('Invalid arguments')

    if(!_.isObject(args.instance))
        throw new Error('Invalid instance')

    if(!(args.target instanceof Element))
        throw new Error('Invalid target')

    if(!_.isObject(args.team))
        throw new Error('A team is expected.')

    if(!_.isString(args.treeName))
        throw new Error('Invalid treeName')

    const groups = []
    _.forEach(args.team.groups, function(v, k){
        const members = []
        _.forEach(v.members, function(v, k){
            const member = teamMember(v)
            members.push(member)
        })
        
        const group = teamGroup({
            groupName: v.group,
            groupMembers: members.join(''),
        })

        groups.push(group)
    })

    const team = teamTree({
        team: groups.join(''),
        treeName: args.treeName,
    })

    const container = htmlToElement(team)
    container.classList.add('container')
    
    if(_.isArray(args.classList)){
        _.forEach(args.classList, function(v){
            container.classList.add(v)
        })
    }

    args.target.appendChild(container)
    args.instance.props.elements.trees.push(container)
    
    addTeamTreeEvents({
        instance: args.instance,
        element: container,
    })

    return container
}